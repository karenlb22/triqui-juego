class Triqui {

    constructor() {

        this.matriz = [
            ["casilla1", "casilla2", "casilla3"],
            ["casilla4", "casilla5", "casilla6"],
            ["casilla7", "casilla8", "casilla9"]
        ];
        this.dibujarTablero();
        this.definirJugador();

    }

    dibujarTablero() {

        //Crear un objeto que referencie a la división tablero
        let miTablero = document.getElementById("tablero");

        for (let i = 0; i < 9; i++) {
            miTablero.innerHTML = miTablero.innerHTML + "<input type='text' id='casilla"+(i+1)+"' class='casilla' onclick='miTriqui.realizarJugada()' style='color: transparent;text-shadow: 0 0 0 blue;' >";
            if((i+1)%3==0){
                miTablero.innerHTML = miTablero.innerHTML + "<br>";
            }

        }
    }

    definirJugador(){
        let n;
        let miTurno = document.getElementById("turno");

        n=Math.round(Math.random()+1);

        if(n===1){
            this.turno="X";
        }else{
            this.turno="O";
        }
        miTurno.innerHTML="Es el turno de: "+ this.turno;                      
    }

    realizarJugada(){
        let miElemento=event.target;
        if(!(miElemento.value==="X" || miElemento.value==="O")){
            miElemento.value=this.turno;
            this.modificarMatriz(miElemento.id);


            if(this.verificarTriki()===true){
                document.getElementById("resultado").innerHTML="¡"+this.turno+" ES EL GANADOR!";
    
            }else{
                this.cambiarTurno();
                if(this.verificarTriki()===false){
                    document.getElementById("resultado").innerHTML="AUN NO HAY GANADOR";
                }else{
                    this.cambiarTurno();
                }
            }
        }else{
            document.getElementById("error").innerHTML="La casilla esta llena";
        }
        
    }

    modificarMatriz(id){

        for(let i=0; i<3; i++){  
            if(id === this.matriz[0][i]){
                    this.matriz[0][i] = this.turno;                
            }
        }
        for(let i=0; i<3; i++){  
            if(id === this.matriz[1][i]){
                    this.matriz[1][i] = this.turno;                
            }
        }
        for(let i=0; i<3; i++){  
            if(id === this.matriz[2][i]){
                    this.matriz[2][i] = this.turno;                
            }
        } 
    }

    cambiarTurno(){
        let miTurno = document.getElementById("turno");
        if(this.turno==="X"){
            this.turno="O";
        }else{
            this.turno="X";
        }
        miTurno.innerHTML="Es el turno de: "+ this.turno; 
    }

    verificarTriki() {

        let triki=false;

        for (let fila = 0; fila < 3; fila++) {
            if (this.matriz[fila][0] === this.matriz[fila][1] && this.matriz[fila][0] === this.matriz[fila][2]) {
                triki = true;
            }
        }

        for (let columna = 0; columna < 3; columna++) {
            if (this.matriz[0][columna] === this.matriz[1][columna] && this.matriz[0][columna] === this.matriz[2][columna]) {
                triki = true;
                return triki;
            }
        }

        if (this.matriz[0][0] === this.matriz[1][1] && this.matriz[0][0] === this.matriz[2][2]) {
            triki = true;
            return triki;
        }

        if (this.matriz[0][2] === this.matriz[1][1] && this.matriz[0][2] === this.matriz[2][0]) {
            triki = true;
            return triki;
        }
        return triki;
    }
    reiniciar(){

        this.matriz=[
            ["casilla1", "casilla2", "casilla3"],
            ["casilla4", "casilla5", "casilla6"],
            ["casilla7", "casilla8", "casilla9"]
        ];
        
        for(let i=0;i<9;i++){
            let campo =document.getElementById("casilla"+(i+1));
            campo.value=" "; 
        }

        let r =document.getElementById("resultado");
        r.innerHTML=" ";

        let c = document.getElementById("error");
        c.innerHTML=" ";
    }
}



